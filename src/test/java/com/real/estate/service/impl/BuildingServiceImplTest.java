package com.real.estate.service.impl;

import com.real.estate.exeption.RequestException;
import com.real.estate.model.PropertyType;
import com.real.estate.model.entity.Address;
import com.real.estate.model.entity.Building;
import com.real.estate.model.entity.Owner;
import com.real.estate.repository.BuildingRepository;
import com.real.estate.service.OwnerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BuildingServiceImplTest {

    @Mock
    private BuildingRepository buildingRepository;

    @Mock
    private OwnerService ownerService;

    @InjectMocks
    private BuildingServiceImpl buildingService;

    private Building building;

    private Owner owner;

    @BeforeEach
    public void setUp() {
        owner = new Owner(1L, "firstName", "lastName",
                "email@mail.com", null, null);
        Address address = new Address("city", "street", "56A");
        building = new Building(1L, address, owner, 50.5, 120.45, PropertyType.APARTMENT);
    }

    @Test
    public void shouldFindAll() {
        //given
        //when
        buildingService.getBuildings();

        //then
        verify(buildingRepository).findAll();
    }

    @Test
    public void shouldFindById() {
        //given
        when(buildingRepository.findById(anyLong())).thenReturn(Optional.of(building));

        //when
        Building returnedBuilding = buildingService.getBuildingById(anyLong());

        //then
        assertEquals(returnedBuilding, building);
        verify(buildingRepository).findById(anyLong());
    }

    @Test
    public void shouldThrowErrorIfIdNotPresent() {
        //given
        when(buildingRepository.findById(anyLong())).thenReturn(Optional.empty());

        //when
        RequestException exception =
                assertThrows(RequestException.class, () -> buildingService.getBuildingById(0L));

        //then
        assertEquals(exception.getMessage(), "Can not find building with id " + 0L);
    }

    @Test
    public void shouldCreateBuilding() {
        //given
        when(ownerService.getOwner(any(Owner.class))).thenReturn(owner);

        //when
        buildingService.createBuilding(building);

        //then
        verify(buildingRepository).save(building);
    }

    @Test
    public void shouldThrowErrorIfOwnerNotPresent() {
        //given
        building.setOwner(null);

        //when
        RequestException requestException =
                assertThrows(RequestException.class, () -> buildingService.createBuilding(building));

        //then
        assertEquals(requestException.getMessage(), "Please add owner id or full information");
    }

    @Test
    public void shouldUpdateBuilding() {
        //given
        when(buildingRepository.findById(anyLong())).thenReturn(Optional.of(building));
        when(ownerService.getOwner(any(Owner.class))).thenReturn(owner);

        //when
       buildingService.updateBuilding(building.getId(), building);

        //then
        verify(buildingRepository).save(any(Building.class));
    }

    @Test
    public void shouldDeleteBuilding() {
        //given
        //when
        buildingService.deleteBuilding(anyLong());

        //then
        verify(buildingRepository).deleteById(anyLong());
    }
}