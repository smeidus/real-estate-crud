package com.real.estate.service.impl;

import com.real.estate.exeption.RequestException;
import com.real.estate.model.PropertyType;
import com.real.estate.model.entity.Address;
import com.real.estate.model.entity.Building;
import com.real.estate.model.entity.Owner;
import com.real.estate.repository.OwnerRepository;
import com.real.estate.validator.OwnerValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OwnerServiceImplTest {

    @Mock
    private OwnerRepository ownerRepository;

    @Mock
    private OwnerValidator ownerValidator;

    @InjectMocks
    private OwnerServiceImpl ownerService;

    private Owner owner;

    @BeforeEach
    public void setUp() {
        List<Building> buildings = new ArrayList<>();
        Address address1 = new Address("city", "street", "56A");
        Address address2 = new Address("city1", "street1", "56B");
        Building building1 = new Building(1L, address1, owner, 50.5, 12000.00, PropertyType.APARTMENT);
        Building building2 = new Building(2L, address2, owner, 55.5, 12500.00, PropertyType.APARTMENT);
        buildings.add(building1);
        buildings.add(building2);
        owner = new Owner(1L, "firstName", "lastName",
                "email@mail.com", "111", buildings);
    }

    @Test
    public void shouldFindAll() {
        //given
        //when
        ownerService.getOwners();

        //then
        verify(ownerRepository).findAll();
    }

    @Test
    public void shouldFindById() {
        //given
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(owner));

        //when
        Owner returnedOwner = ownerService.getOwnerById(anyLong());

        //then
        assertEquals(returnedOwner, owner);
        verify(ownerRepository).findById(anyLong());
    }

    @Test
    public void shouldThrowErrorIfIdNotPresent() {
        //given
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.empty());

        //when
        RequestException exception =
                assertThrows(RequestException.class, () -> ownerService.getOwnerById(0L));

        //then
        assertEquals(exception.getMessage(), "Can not find owner with id " + 0L);
    }

    @Test
    public void shouldFindOwnedBuildings() {
        //given
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(owner));

        //when
        List<Building> returnedBuildings = ownerService.getOwnedBuildings(anyLong());

        //then
        assertEquals(returnedBuildings.size(), 2);

    }

    @Test
    public void shouldGetOwnerWhenIdProvided() {
        //given
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(owner));

        //when
        Owner returnedOwner = ownerService.getOwner(owner);

        //then
        assertEquals(returnedOwner, owner);
    }

    @Test
    public void shouldCreateNewOwnerIfIdNull() {
        //given
        owner.setId(null);
        when(ownerRepository.save(owner)).thenReturn(owner);

        //when
        Owner returnedOwner = ownerService.getOwner(owner);

        //then
        assertEquals(returnedOwner, owner);
    }

    @Test
    public void shouldCreateNewOwnerIfIdNotExist() {
        //given
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.empty());
        when(ownerRepository.save(owner)).thenReturn(owner);

        //when
        Owner returnedOwner = ownerService.getOwner(owner);

        //then
        assertEquals(returnedOwner, owner);
    }

    @Test
    public void shouldCalculateBuildingsTax() {
        //given
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(owner));

        //when
        Double taxValue = ownerService.calculateTax(owner.getId());

        //then
        assertEquals(taxValue, 49);

    }

    @Test
    public void shouldUpdateOwner() {
        //given
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(owner));

        //when
        ownerService.updateOwner(owner.getId(), owner);

        //then
        verify(ownerRepository).save(any(Owner.class));
    }

    @Test
    public void shouldDeleteOwner() {
        //given
        //when
        ownerService.deleteOwner(anyLong());

        //then
        verify(ownerRepository).deleteById(anyLong());
    }
}