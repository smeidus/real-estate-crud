package com.real.estate.validator;

import com.real.estate.exeption.RequestException;
import com.real.estate.model.entity.Owner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OwnerValidatorTest {

    private OwnerValidator ownerValidator = new OwnerValidator();

    @Test
    void shouldThrowExceptionIfFirstNameBlank() {

        //given
        Owner owner = new Owner(1L, "", "lastName",
                "email@mail.com", null, null);

        //when
        RequestException exception = assertThrows(RequestException.class,
                () -> ownerValidator.validateOwnerDetails(owner));

        //then
        assertEquals(exception.getMessage(), "First Name may not be null or blank");
    }

    @Test
    void shouldThrowExceptionIfLastNameNull() {

        //given
        Owner owner = new Owner(1L, "firstName", null,
                "email@mail.com", null, null);

        //when
        RequestException exception = assertThrows(RequestException.class,
                () -> ownerValidator.validateOwnerDetails(owner));

        //then
        assertEquals(exception.getMessage(), "Last Name may not be null or blank");
    }

    @Test
    void shouldThrowExceptionIfEmailBlank() {

        //given
        Owner owner = new Owner(1L, "firstName", "lastName",
                "", null, null);

        //when
        RequestException exception = assertThrows(RequestException.class,
                () -> ownerValidator.validateOwnerDetails(owner));

        //then
        assertEquals(exception.getMessage(), "Email may not be null or blank");
    }
}