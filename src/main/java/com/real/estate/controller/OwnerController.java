package com.real.estate.controller;

import com.real.estate.model.entity.Building;
import com.real.estate.model.entity.Owner;
import com.real.estate.service.OwnerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/owner")
@Api(tags = {"Owner API"})
@SwaggerDefinition(tags = {
        @Tag(name = "Owner API", description = "Provides basic CRUD operations")
})
public class OwnerController {

    @Autowired
    private OwnerService ownerService;

    @GetMapping("/get/{id}")
    public ResponseEntity<Owner> getOwner(@PathVariable Long id) {
        Owner owner = ownerService.getOwnerById(id);
        return new ResponseEntity<>(owner, HttpStatus.OK);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<Owner>> getOwners() {
        List<Owner> owners = ownerService.getOwners();
        return new ResponseEntity<>(owners, HttpStatus.OK);
    }

    @GetMapping("/get/buildings/{id}")
    public ResponseEntity<List<Building>> getBuildingByOwner(@PathVariable Long id) {
        List<Building> buildings = ownerService.getOwnedBuildings(id);
        return new ResponseEntity<>(buildings, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Owner> createOwner(@RequestBody Owner owner) {
        Owner newOwner = ownerService.createOwner(owner);
        return new ResponseEntity<>(newOwner, HttpStatus.OK);
    }

    @GetMapping("/calculateTax/{id}")
    public ResponseEntity<Double> calculateTax(@PathVariable Long id) {
        Double calculateTax = ownerService.calculateTax(id);
        return new ResponseEntity<>(calculateTax, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Owner> updateOwner(@PathVariable Long id,
                                             @RequestBody Owner owner) {
        Owner updateOwner = ownerService.updateOwner(id, owner);
        return new ResponseEntity<>(updateOwner, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteOwner(@PathVariable Long id) {
        ownerService.deleteOwner(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
