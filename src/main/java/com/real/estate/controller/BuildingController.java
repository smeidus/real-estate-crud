package com.real.estate.controller;

import com.real.estate.model.entity.Building;
import com.real.estate.service.BuildingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/building")
@Api(tags = {"Building API"})
@SwaggerDefinition(tags = {
        @Tag(name = "Building API", description = "Provides basic CRUD operations")
})
@EnableTransactionManagement
public class BuildingController {

    @Autowired
    private BuildingService buildingService;

    @GetMapping("/get/{id}")
    public ResponseEntity<Building> getBuilding(@PathVariable Long id) {
        Building building = buildingService.getBuildingById(id);
        return new ResponseEntity<>(building, HttpStatus.OK);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<Building>> getBuildings() {
        List<Building> buildings = buildingService.getBuildings();
        return new ResponseEntity<>(buildings, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Building> addBuilding(@Valid @RequestBody Building building) {
        Building newBuilding = buildingService.createBuilding(building);
        return new ResponseEntity<>(newBuilding, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Building> updateBuilding(@PathVariable Long id,
                                                   @Valid @RequestBody Building building) {
        Building updatedBuilding = buildingService.updateBuilding(id, building);
        return new ResponseEntity<>(updatedBuilding, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteBuilding(@PathVariable Long id) {
        buildingService.deleteBuilding(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
