package com.real.estate.validator;

import com.real.estate.exeption.RequestException;
import com.real.estate.model.entity.Owner;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OwnerValidator {

    public void validateOwnerDetails(Owner owner) {

        String message = "may not be null or blank";

        Optional.ofNullable(owner.getFirstName())
                .filter(name -> !name.isBlank())
                .orElseThrow(() -> new RequestException("First Name " + message));

        Optional.ofNullable(owner.getLastName())
                .filter(name -> !name.isBlank())
                .orElseThrow(() -> new RequestException("Last Name " + message));

        Optional.ofNullable(owner.getEmail())
                .filter(email -> !email.isBlank())
                .orElseThrow(() -> new RequestException("Email " + message));
    }

}
