package com.real.estate.exeption;

public class RequestException extends RuntimeException {

    public RequestException(String message) {
        super(message);
    }
}
