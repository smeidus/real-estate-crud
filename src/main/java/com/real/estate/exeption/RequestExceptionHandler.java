package com.real.estate.exeption;

import com.real.estate.model.exception.Exception;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RequestExceptionHandler {

    @ExceptionHandler(value = {RequestException.class, ConstraintViolationException.class})
    public ResponseEntity<Object> handleRuntimeException(RuntimeException runtimeException) {

        List<Exception> runtimeExceptions = new ArrayList<>();

        if (runtimeException instanceof RequestException) {
            createRequestException((RequestException) runtimeException, runtimeExceptions);
        }

        if (runtimeException instanceof ConstraintViolationException) {
            createConstraintException(((ConstraintViolationException) runtimeException), runtimeExceptions);
        }

        return new ResponseEntity<>(runtimeExceptions, HttpStatus.BAD_REQUEST);
    }

    private void createConstraintException(ConstraintViolationException exception,
                                           List<Exception> runtimeExceptions) {
        exception.getConstraintViolations()
                .forEach(error -> {
                    Exception customException = Exception.builder()
                            .message(error.getMessage())
                            .status(HttpStatus.BAD_REQUEST)
                            .timestamp(ZonedDateTime.now())
                            .build();

                    runtimeExceptions.add(customException);
                });
    }

    private void createRequestException(RequestException requestException,
                                        List<Exception> runtimeExceptions) {
        Exception exception = Exception.builder()
                .message(requestException.getMessage())
                .status(HttpStatus.BAD_REQUEST)
                .timestamp(ZonedDateTime.now())
                .build();

        runtimeExceptions.add(exception);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleArgumentException(MethodArgumentNotValidException exception) {

        List<Exception> validationExceptions = new ArrayList<>();
        createMethodArgumentException(exception, validationExceptions);

        return new ResponseEntity<>(validationExceptions, HttpStatus.BAD_REQUEST);
    }

    private void createMethodArgumentException(MethodArgumentNotValidException exception,
                                               List<Exception> validationExceptions) {
        exception.getAllErrors().forEach(error -> {
            Exception customException = Exception.builder()
                    .message(error.getDefaultMessage())
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(ZonedDateTime.now())
                    .build();

            validationExceptions.add(customException);
        });
    }

    @ExceptionHandler(value = JdbcSQLIntegrityConstraintViolationException.class)
    public ResponseEntity<Object> handleSQLException(JdbcSQLIntegrityConstraintViolationException exception) {

        String state = "23505";
        Exception customException = null;

        if (state.equals(exception.getSQLState())) {
            customException = Exception.builder()
                    .message("Entity already exists with this email or phone number")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(ZonedDateTime.now())
                    .build();
        }

        return new ResponseEntity<>(customException, HttpStatus.BAD_REQUEST);
    }

}
