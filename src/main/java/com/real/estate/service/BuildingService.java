package com.real.estate.service;

import com.real.estate.model.entity.Building;

import java.util.List;

/**
 * Service for {@link Building} operations
 */
public interface BuildingService {

    Building getBuildingById(Long buildingId);

    List<Building> getBuildings();

    Building createBuilding(Building building);

    Building updateBuilding(Long buildingId, Building building);

    void deleteBuilding(Long buildingId);

}
