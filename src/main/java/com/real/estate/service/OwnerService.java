package com.real.estate.service;

import com.real.estate.model.entity.Building;
import com.real.estate.model.entity.Owner;

import java.util.List;

/**
 * Service for {@link Owner} operations
 */
public interface OwnerService {

    Owner getOwnerById(Long ownerId);

    /**
     * Returns all existing buildings that owner owns
     * @param ownerId id of the owner
     * @return all existing owner buildings
     */
    List<Building> getOwnedBuildings(Long ownerId);

    Owner getOwner(Owner owner);

    List<Owner> getOwners();

    Owner createOwner(Owner owner);

    Owner updateOwner(Long ownerId, Owner owner);

    /**
     * Calculates tax that owner has to pay for amount of buildings he has
     * @param ownerId id of the owner
     * @return calculated tax
     */
    Double calculateTax(Long ownerId);

    void deleteOwner(Long ownerId);

}
