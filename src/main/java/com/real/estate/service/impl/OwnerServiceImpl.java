package com.real.estate.service.impl;

import com.real.estate.exeption.RequestException;
import com.real.estate.model.entity.Building;
import com.real.estate.model.entity.Owner;
import com.real.estate.repository.OwnerRepository;
import com.real.estate.service.OwnerService;
import com.real.estate.validator.OwnerValidator;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class OwnerServiceImpl implements OwnerService {

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private OwnerValidator ownerValidator;

    @Override
    public Owner getOwnerById(Long ownerId) {
        return ownerRepository.findById(ownerId)
                .orElseThrow(() -> new RequestException("Can not find owner with id " + ownerId));
    }

    @Override
    public List<Owner> getOwners() {
        return ownerRepository.findAll();
    }

    @Override
    public List<Building> getOwnedBuildings(Long ownerId) {
        return getOwnerById(ownerId).getOwnedBuildings();
    }

    @Override
    public Owner getOwner(Owner owner) {
        if (owner.getId() != null) {
            return ownerRepository.findById(owner.getId())
                    .orElseGet(() -> {
                        log.info("Owner is not existing, creating new owner");
                        return createOwner(owner);
                    });
        } else {
            return createOwner(owner);
        }
    }

    @Override
    public Owner createOwner(Owner owner) {
        ownerValidator.validateOwnerDetails(owner);
        return ownerRepository.save(owner);
    }

    @Override
    public Owner updateOwner(Long ownerId, Owner owner) {
        Owner existingOwner = getOwnerById(ownerId);
        ownerValidator.validateOwnerDetails(owner);
        existingOwner.setFirstName(owner.getFirstName());
        existingOwner.setLastName(owner.getLastName());
        existingOwner.setEmail(owner.getEmail());
        String phoneNumber = Optional.ofNullable(owner.getPhoneNumber())
                .orElse(existingOwner.getPhoneNumber());
        existingOwner.setPhoneNumber(phoneNumber);
        return ownerRepository.save(existingOwner);
    }

    @Override
    public Double calculateTax(Long ownerId) {
        List<Building> ownedBuildings = getOwnedBuildings(ownerId);
        return ownedBuildings.stream()
                .mapToDouble(building -> building.getMarketValue() * building.getPropertyType().getTaxRate()).sum();
    }

    @Override
    public void deleteOwner(Long ownerId) {
        ownerRepository.deleteById(ownerId);
    }
}
