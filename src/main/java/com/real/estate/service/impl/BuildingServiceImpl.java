package com.real.estate.service.impl;

import com.real.estate.exeption.RequestException;
import com.real.estate.model.entity.Building;
import com.real.estate.model.entity.Owner;
import com.real.estate.repository.BuildingRepository;
import com.real.estate.service.BuildingService;
import com.real.estate.service.OwnerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class BuildingServiceImpl implements BuildingService {

    @Autowired
    private BuildingRepository buildingRepository;

    @Autowired
    private OwnerService ownerService;

    @Override
    public Building getBuildingById(Long buildingId) {
        return buildingRepository.findById(buildingId)
                .orElseThrow(() -> new RequestException("Can not find building with id " + buildingId));
    }

    @Override
    public List<Building> getBuildings() {
        return buildingRepository.findAll();
    }

    @Override
    @Transactional
    public Building createBuilding(Building building) {
        Owner currentOwner = getOwner(building);
        building.setOwner(currentOwner);
        return buildingRepository.save(building);
    }

    private Owner getOwner(Building building) {
        return Optional.ofNullable(building.getOwner())
                .map(owner -> ownerService.getOwner(owner))
                .orElseThrow(() -> new RequestException("Please add owner id or full information"));
    }

    @Override
    public Building updateBuilding(Long buildingId, Building building) {
        Building existingBuilding = getBuildingById(buildingId);
        existingBuilding.setAddress(building.getAddress());
        existingBuilding.setMarketValue(building.getMarketValue());
        existingBuilding.setSize(building.getSize());
        existingBuilding.setPropertyType(building.getPropertyType());
        Owner owner = getOwner(building);
        existingBuilding.setOwner(owner);
        return buildingRepository.save(existingBuilding);
    }

    @Override
    public void deleteBuilding(Long buildingId) {
        buildingRepository.deleteById(buildingId);
    }

}
