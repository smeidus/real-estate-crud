package com.real.estate.model.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

@AllArgsConstructor
@Data
@Builder
public class Exception {

    private String message;

    private HttpStatus status;

    private ZonedDateTime timestamp;
}
