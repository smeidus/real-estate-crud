package com.real.estate.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PropertyType {

    APARTMENT(0.002), HOUSE(0.003), INDUSTRIAL(0.005);

    private final double taxRate;
}
