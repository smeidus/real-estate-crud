package com.real.estate.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.real.estate.model.PropertyType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Building {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Valid
    @Embedded
    @NotNull(message = "Address may not be null")
    private Address address;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    @JsonBackReference
    private Owner owner;

    @NotNull(message = "Size may not be null")
    private Double size;

    @NotNull(message = "Market value may not be null")
    private Double marketValue;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Property type may not be null")
    private PropertyType propertyType;

}
