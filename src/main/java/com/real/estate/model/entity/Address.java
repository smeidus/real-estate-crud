package com.real.estate.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    @NotBlank(message = "City may not be null")
    private String city;

    @NotBlank(message = "Street may not be null")
    private String street;

    @NotBlank(message = "House number may not be null")
    private String houseNumber;
}
