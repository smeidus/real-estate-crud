# How to run app

### First method
- Open project with favorite IDE and start application  

### Second method
1. In main folder run command `mvn clean install`  
2. Go to `target` directory and run command `java -jar estate-0.0.1-SNAPSHOT.jar`

---

# Links

[Swagger](http://localhost:8080/swagger-ui/#/)  

[H2](http://localhost:8080/h2-console/)

